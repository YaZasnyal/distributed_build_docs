# dev_toolchain
Основой данной сборки является [dev_toolchain](https://gitlab.com/YaZasnyal-docker/dev_toolchain) - набор программ и библиотек, необходимых для разработки проектов компании. Данный набор собирается отдельно от виртуальной машины, поэтому его можно обновлять отдельно, но все изменения оперативно отражаются в мастер-образе виртуальной машины. Для обновления нужно скачать актуальную сборку из pipeline'ов и распаковать ее в ```/opt``` с заменой файлов (лучше удалить старую версию полностью). Для удобства был сделан скрипт, который можно вызвать следующим образом:

```sh
wget -O - https://redirects.yazasnyal.dev/install_dev_toolchain.sh | sudo bash
```

Также существует docker контейнер с этим этим набором: ```registry.gitlab.com/yazasnyal-docker/dev_toolchain:latest```

Состав:

* dev_toolchain:
    * gcc: 8.3
    * binutils: 2.3.6
    * gdb: 10.2
    * python: 3.8.9
    * git: 2.32
    * cmake: 3.20.5
    * ninja: 1.10.2
    * log4cplus: 1.2.x
    * boost: 1.75.0
    * fpc: 3.2.2
    * tarantool: 2.7.2
    * icecc - 1.3.1
    * ccache - 4.4.2
